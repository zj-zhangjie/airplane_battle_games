import pygame

class Bullet1(pygame.sprite.Sprite):   # 第一种子弹类型
    """docstring for Bullet1"""
    def __init__(self, position):
        super(Bullet1, self).__init__()

        self.image = pygame.image.load("images/bullet1.png").convert_alpha()  # 设置子弹形状
        # self.image包含top bottom left right
        self.rect = self.image.get_rect()  # 图像大小
        self.rect.left, self.rect.top = position    # 初始化位置
        self.speed = 12 # 设置子弹射击范围
        self.active = True    # 是否显示？
        self.mask = pygame.mask.from_surface(self.image) # ？

    def move(self):
        self.rect.top -= self.speed
        if self.rect.top < 0:
            self.active = False

    def reset(self, position):   # 刷新位置？
        self.rect.left, self.rect.top = position
        self.active = True

class Bullet2(pygame.sprite.Sprite):
    """docstring for Bullet2"""
    def __init__(self, position):
        super(Bullet2, self).__init__()

        self.image = pygame.image.load("images/bullet2.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = position
        self.speed = 14  # 子弹更快一点
        self.active = False
        self.mask = pygame.mask.from_surface(self.image)

    def move(self):
        self.rect.top -= self.speed
        if self.rect.top < 0:
            self.active = False

    def reset(self, position):
        self.rect.left, self.rect.top = position
        self.active = True